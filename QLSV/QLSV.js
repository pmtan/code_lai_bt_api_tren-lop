var BASE_URL = "https://62f8b755e0564480352bf426.mockapi.io/sv";

let getDataAndRender = () =>{
    axios({
        url:`${BASE_URL}`,
        method: "GET",
    })
    .then(function(res){
        dssv = res.data;
        renderDanhSachSinhVien(dssv);
    })
    .catch(function(err){
        console.log(err);
    });
}
getDataAndRender();


// Thêm Sinh Viên:
let themSV = () =>{
    let newSinhVien = layThongTinTuForm();
    loadingOn();
    axios({
        url:`${BASE_URL}`,
        method: "POST",
        data: newSinhVien,
    })
    .then(function(res){
        loadingOff();
        console.log(res.data);
        getDataAndRender();
    })
    .catch(function(err){
        loadingOff();
        console.log(err);
    })
}


// Xoá Sinh Viên:
let xoaSV = (maSV) => {
    loadingOn();
    axios({
        url: `${BASE_URL}/${maSV}`,
        method: "DELETE",
    })
    .then(function(res){
        loadingOff();
        console.log(res);
        getDataAndRender();    
    })
    .catch(function(err){
        loadingOff();
        console.log(err);
    });
}


// Sửa Sinh Viên:
let suaSV = (maSV) => {
    axios({
        url: `${BASE_URL}/${maSV}`,
        method: "GET",
    })
    .then(function(res){
        let sv = res.data;
        console.log(sv);
        showThongTin(sv);   
    })
    .catch(function(err){
        console.log(err);
    });
}


// Cập Nhật Sinh Viên:
let capNhatSV = () => {
    loadingOn();
    let svEdited = layThongTinTuForm();
    axios({
        url:`${BASE_URL}/${svEdited.ma}`,
        method: "PUT",
        data: svEdited,
    })
    .then(function(res){
        loadingOff();
        console.log(res.data);
        getDataAndRender();
    })
    .catch(function(err){
        loadingOff();
        console.log(err);
    })
}



