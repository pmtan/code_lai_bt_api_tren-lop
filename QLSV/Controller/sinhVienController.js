function SinhVien(_ma, _ten, _email, _hinhAnh){
    this.ma = _ma;
    this.ten = _ten;
    this.email = _email;
    this.hinhAnh = _hinhAnh;
}

function renderDanhSachSinhVien(list){
    var tableHTML = "";
    for (index = 0; index < list.length; index++){
        var tableRow = `
        <tr>
        <td>${list[index].ma}</td>
        <td>${list[index].ten}</td>
        <td>${list[index].email}</td>
        <td><img style="width:50px; border-radius:8px" src="${list[index].hinhAnh}" alt=""/></td>
        <td>
        <button onclick="suaSV(${list[index].ma})" class="btn btn-warning">Sửa</button>
        <button onclick="xoaSV(${list[index].ma})" class="btn btn-danger">Xoá</button>
        </td>
        </tr>
        `
        tableHTML+= tableRow;
    };
    document.getElementById("tbodySinhVien").innerHTML = tableHTML;
}

function showThongTin(sinhVien){
    document.getElementById("txtMaSV").value = sinhVien.ma;
    document.getElementById("txtTenSV").value = sinhVien.ten;
    document.getElementById("txtEmail").value = sinhVien.email;
    document.getElementById("txtImg").value = sinhVien.hinhAnh;
}

function layThongTinTuForm(){
    let maSV = document.getElementById("txtMaSV").value;
    let tenSV = document.getElementById("txtTenSV").value;
    let emailSV = document.getElementById("txtEmail").value;
    let hinhAnhSV = document.getElementById("txtImg").value;
    return new SinhVien(maSV, tenSV, emailSV, hinhAnhSV);
}

let loadingOn = () => {
    document.getElementById("loading").style.display = "flex";
}
let loadingOff = () => {
    document.getElementById("loading").style.display = "none";
}